#Makefile
#Jonathan Gorotiza
#Andrea Naranjo

prog: 
	gcc -Wall programa.c -o prog -lpthread

.PHONY: clean
clean:
	rm prog
