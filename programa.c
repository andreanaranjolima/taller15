#include <stdio.h>
#include <semaphore.h>
#include <getopt.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>

typedef struct{
	int *buf;
	int n;
	int front;
	int rear;
	sem_t mutex;
	sem_t slots;
	sem_t items;
} sbuf_t;

typedef struct{
	int *buf;
	int n;
	int front;
	int rear;
} buf_t;

void *thread1(void *vargp);
void *thread2(void *vargp);
void *thread3(void *vargp);
void *thread4(void *vargp);
int sbuf_remove(sbuf_t *sp);
void sbuf_init(sbuf_t *sp, int n);
void buf_init(buf_t *sp, int n);
void sbuf_deinit(sbuf_t *sp);
void sbuf_insert(sbuf_t *sp, int item);
void buf_deinit(buf_t *sp);

sbuf_t buffer;
buf_t buff;
sem_t mut;

int main(int args, char **argv){

	pthread_t tid_prod;
	pthread_t tid_cons;
	sem_init(&mut, 0, 1);
	
	if(args < 9 || args > 10){
		printf("Uso: prog -p # -c # -n # -e # [sync]\n");
		return -1;
	}

	int prod = atoi(argv[2]);
	int cons = atoi(argv[4]);
	int n = atoi(argv[6]);
	int elem = atoi(argv[8]);

	if(elem > n){
		printf("Número de elementos mayor al tamaño del buffer\n");
		return -1;
	}
	
	
	if(args == 9){
		buf_init(&buff, n);
		for(int i = 0; i < prod; i++){
			pthread_create(&tid_prod, NULL, thread3, &i);
		}
		for(int j = 0; j < cons; j++){
			pthread_create(&tid_cons, NULL, thread4, &j);
		}

		buf_deinit(&buff);
	}
	if(args == 10){
		sbuf_init(&buffer, n);
		for(int i = 0; i < prod; i++){
			pthread_create(&tid_prod, NULL, thread1, &i);
		}
		for(int j = 0; j < cons; j++){
			pthread_create(&tid_cons, NULL, thread2, &j);
		}
			
		sbuf_deinit(&buffer);
	
	}
	
	return 1;
}

void sbuf_init(sbuf_t *sp, int n){
	sp->buf = calloc(n, sizeof(int));
	sp->n = n;
	sp->front = sp->rear = 0;
	sem_init(&sp->mutex,0,1);
	sem_init(&sp->slots,0,n);
	sem_init(&sp->items,0,0);
}

void sbuf_deinit(sbuf_t *sp){
	free(sp->buf);
}

void sbuf_insert(sbuf_t *sp, int item){
	sem_wait(&sp->slots);
	sem_wait(&sp->mutex);
	sp->buf[(++sp->rear)%(sp->n)] = item;
	sem_post(&sp->mutex);
	sem_post(&sp->items);
}

int sbuf_remove(sbuf_t *sp){
	int item;
	sem_wait(&sp->items);
	sem_wait(&sp->mutex);
	item = sp->buf[(++sp->front)%(sp->n)];
	sem_post(&sp->mutex);
	sem_post(&sp->slots);
	return item;
}

void buf_init(buf_t *sp, int n){
	sp->buf = calloc(n, sizeof(int));
	sp->n = n;
	sp->front = sp->rear = 0;
}

void buf_deinit(buf_t *sp){
	free(sp -> buf);
}

void *thread1(void *vargp){
	pthread_detach(pthread_self());
	int m = *((int *)vargp);
	int al = rand() % 101;
	
		sbuf_insert(&buffer, al);
		usleep(1);
		sem_wait(&mut);
		printf("Hilo %d produjo un item. Hay %d items en cola\n", m, (buffer.n)-(buffer.rear));
		sem_post(&mut);
	
	return NULL;
}

void *thread2(void *vargp){
	pthread_detach(pthread_self());
	int m = *((int *)vargp);
	
	sbuf_remove(&buffer);
	usleep(3);
	sem_wait(&mut);
	printf("Hilo %d consumio un item. Hay %d items en cola\n", m, (buffer.n)-(buffer.front));
	sem_post(&mut);
	
	return NULL;
}

void *thread3(void *vargp){
	pthread_detach(pthread_self());
	int m = *((int *)vargp);
	int item = rand() % 101;
	buff.buf[(++buff.rear)%(buff.n)] = item;
	printf("Hilo %d produjo un item. Hay %d items en cola\n", m, (buffer.n)-(buffer.rear));
	return NULL;
}

void *thread4(void *vargp){
	pthread_detach(pthread_self());
	int m = *((int *)vargp);
	int item = buff.buf[(++buff.front)%(buff.n)];
	printf("Item: %d\n",item);
	printf("Hilo %d consumio un item. Hay %d items en cola\n", m, (buffer.n)-(buffer.front));
	return NULL;
}
